#!/bin/bash

# Needs:
#
# sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# Remerciements
###############

# This whole script was inspired from the great https://gitlab.com/python-devs/ci-images/, so Thanks Barry Warsaw.

# The automatic poking of last version is greatly inspired by z4c, thanks!


get-python-version()
{
    # Lists the /ftp/python/ directory searching for the most recent
    # Python version directory matching a given pattern.

    # Exemples:
    #
    # $ get-python-versions 3
    # 3.13.0
    # $ get-python-versions 3.10
    # 3.10.14

    # Such that /ftp/python/$(get-python-version 3.12) exists.

    local URL="https://www.python.org/ftp/python/"
    local PY_VERSION="$1"

    wget -qO- "$URL" |
        grep -o ">[0-9.]\+/<" |
        sed "s/^>//;s|/<$||" |
        sort --sort=version |
        grep "^$PY_VERSION\." |
        tail -n 1
}

get-python-version-suffix()
{
    # Given a directory from /ftp/python/ like `3.12.0` gets the most
    # recent version suffix for it.

    # Examples:
    #
    # $ get-python-version-suffix 3.10.3
    # # prints nothing, there's no pre-release for it.
    #
    # $ get-python-version-suffix 3.12.0
    # # prints nothing, the last 3.12.0 IS 3.12.0
    #
    # $ get-python-version-suffix 3.13.0
    # a5

    local VERSION="$1"

    versions="$(wget -qO- "https://www.python.org/ftp/python/$VERSION/" |
        grep -o '>Python-[0-9]\.[0-9]\+\.[a-z0-9]\+.tgz<' |
        sed "s/^>Python-//;s|.tgz<$||" |
        sort --sort=version)"
    if ! printf "%s" "$versions" | grep --quiet "^$VERSION$"
    then
        printf "%s" "$versions" | tail -n 1 | sed "s/^$VERSION//"
    fi
}

compile-python()
{
    # Compile the given Python version. Accepts a "version pattern" as a filter, like:

    # compile-python 3  # The most recent Python 3 available
    #                   # Beware it may pick an alpha version!

    # compile-python 3.12  # Will compile the most recent 3.12 available
    #                      # Can pick alpha/beta/rc versions if 3.12 is not released yet.

    # compile-python 3.12.2  # Will compile the given version.

    local PY_VERSION
    local SUFFIX
    local URL="https://www.python.org/ftp/python"

    PY_VERSION="$(get-python-version "$1")"
    SUFFIX="$(get-python-version-suffix "$PY_VERSION")"
    (
        cd /tmp || return 1
        wget -qO- "$URL/$PY_VERSION/Python-$PY_VERSION$SUFFIX.tgz" | tar -xzf - || (
            echo "Version not found, check on $URL."
        )
        [ -d "Python-$PY_VERSION$SUFFIX" ] && (cd "Python-$PY_VERSION$SUFFIX"; ./configure --prefix="$HOME/.local/" && make -j "$(nproc)" && make altinstall) &&
            rm -r "Python-$PY_VERSION$SUFFIX"
    )
}

compile-pythons()
{
    # Compiles a usefull set of Python versions.
    compile-python 3.7 &
    compile-python 3.8 &
    compile-python 3.9 &
    compile-python 3.10 &
    compile-python 3.11 &
    compile-python 3.12 &
    compile-python 3.13 &
    wait
}

_compile_python()
{
    COMPREPLY=( $( compgen -W '$( command curl -s https://www.python.org/ftp/python/  | grep -o ">[0-9.]\+/<" | sed "s/^>//;s|/<$||" )' -- "${COMP_WORDS[COMP_CWORD]}") )
}
complete -F _compile_python compile-python
